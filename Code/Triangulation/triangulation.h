/******************************************************************************
 *  @FILE       triangulation.h
 *  @PROJECT    MARS LANDING SYSTEM SPACE APPS CHALLENGE 2018
 *  @SUBTASK	Positioning Software
 *  @TEAM       Thumbling's Travels
 *  @AUTHOR     Evangelos Petrongonas  (vpetrog) (vpetrog@yahoo.gr)
 *  @BRIEF      Header file and documentation for the triangulation API
 *  @BUGS       No known bugs.
 *  @CHANGELOG
 *      -v0.1 Initial Commit
 *****************************************************************************/
#ifndef TRIANGULATION_H_
#define TRIANGULATION_H_

#include "triangulation_defs.h"
#include "antennas_defs.h"

typedef unsigned char u8;
/**
 * initialise_map initialises the map
 * @param MAX_X maximum distance in X Axis
 * @param MAX_Y maximum distance in Y Axis
 * @param map 	the array to be initialised
 */
void initialise_map(const mlsGrid MAX_X, const mlsGrid MAX_Y, mlsGrid map[MAX_Y][MAX_X]);

/**
 * print_map outputs the map for debugging purposes
 * @param MAX_X  maximum distance in X Axis
 * @param MAX_Y  maximum distance in Y Axis
 * @param map 	 the antenna coverage map
 */
void print_map(const mlsGrid MAX_X, const mlsGrid MAX_Y, mlsGrid map[MAX_Y][MAX_X]);

/**
 * distance calculate the L1 norm distance of two points from the grid
 * @param  antenna a Points structs that holds the position of the antenna
 * @param  p       a Points structs that holds the position of a point
 * @return         the L1 norm
 */
float distance(struct Points antenna, struct Points p);

/**
 * creates a map based on the signal received by the antennas and
 * repressents the coverage on a grid system
 * @param noAnt    number of Antennas
 * @param antennas the antennas position array
 * @param antId    the antennas ID array
 * @param antRange the antennas Range
 * @param MAX_X    maximum distance in X Axis
 * @param MAX_Y    maximum distance in Y Axis
 * @param map a 2D array to fill the map
 */
void create_map(struct AntennaInfo antInfo, const mlsGrid MAX_X, const mlsGrid MAX_Y,
			     mlsGrid map[MAX_Y][MAX_X]);

/**
 * calculates using triangulation the astronauts location
 * @param  MAX_X  maximum distance in X Axis
 * @param  MAX_Y  maximum distance in Y Axis
 * @param  map 	  The antennas coverage map
 * @return        a Point struct that hold the astronauts location
 */
struct Points calculate_position(const mlsGrid MAX_X, const mlsGrid MAX_Y, mlsGrid map[MAX_X][MAX_Y]);

/**
 * Prints the output to a file for further visualisation and process
 * The output is printed as follows:
 * the first line is the astronaut's
 * Location in X Y. The following lines consist of the antenna number the,
 * Location and the Range. So the format is
 * L X Y
 * A0 X Y R
 * A1 X Y R
 * etc
 * @param p        the astronauts point
 * @param antennas the antennas position array
 * @param noAnt    number of Antennas
 * @param antRange the antennas Range
 */
void print_output(struct Points p, struct Points* antennas, mlsAntNo noAnt, mlsAntRange* antRange);

/**
 * retuns the number of antennas in range
 * @param i the expected coverage of antennas in grid
 * @return number of antennas
 */
mlsAntNo _decode_ant_number(mlsGrid i);
#endif
