#!/usr/bin/env python

#/******************************************************************************
# *  @FILE       kyklos.py
# *  @PROJECT    MARS LANDING SYSTEM SPACE APPS CHALLENGE 2018
# *  @SUBTASK    Grid System Visualisation
# *  @TEAM       Thumbling's Travels
# *  @AUTHOR     Giannis Politopoulos
# *  @AUTHOR     Evangelos Petrongonas (vpetrog) (vpetrog@hotmail.com)
# *  @BRIEF      Visualisation of the Antenna Grid
# *  @BUGS       None known.
# *  @CHANGELOG
# *      -v0.2 Integration with the triangulation code
# *      -v0.1 Initial Commit

import matplotlib.pyplot as plt
import numpy as np
import subprocess as sp


def create_ant_db(c2):
    f = open("antennas_db.h","w")

    f.write("/**************************************************************\n")
    f.write("\t\t\t\tWARNING: THIS IS AN AUTO GENERATED FILE\n")
    f.write("**************************************************************/\n")
    f.write("\n\n")
    f.write("#include \"mls_types.h\"\n\n ")
    f.write("#ifndef ANTENNAS_DB_H_\n")
    f.write("#define ANTENNAS_DB_H_\n\n")
    f.write("#define ANTENNA_ERR ((mlsGrid) 0)\n")
    f.write("#define TOTAL_ANTENNAS %d\n\n" %len(c2))

    f.write("/* ANTENNA LOCATIONS DEFINITIONS */\n")
    for i, value in enumerate(c2):
        f.write("\n")
        f.write("#define ANTENNA_X_%d" %i)
        f.write("\t(mlsGrid) %d\n" % (int(value[0]) + AX_OFFSET))
        f.write("#define ANTENNA_Y_%d" %i)
        f.write("\t(mlsGrid) %d\n" % (int(value[1]) + AX_OFFSET))

    f.write("\n\n")
    f.write("/* ANTENNA RANGE DEFINITIONS */\n")
    for i, value in enumerate(c2):
        f.write("#define ANTENNA_RANGE_%d" %i)
        f.write("\t(mlsAntRange) %d\n" %RADIUS)

    f.write("\n\n")
    f.write("/* ANTENNA ID DEFINITIONS */\n")
    for i, value in enumerate(c2):
        f.write("#define ANTENNA_ID_%d" %i)
        f.write("\t(mlsAntId) %d\n" % (1<<i))

    f.write("#endif\n")
    f.close()

def read_triangulation_output():
    c = []
    f = open("c_out.dat","r")
    loc = f.readline().split()
    p = (int(loc[1]) - AX_OFFSET, int(loc[2]) - AX_OFFSET)
    line = f.readline()
    while line:
        loc = line.split()
        c.append((int(loc[1]) - AX_OFFSET, int(loc[2]) - AX_OFFSET))
        line = f.readline()
    f.close()
    return  c, p

print("Visualiser Started\n")

# Constants
RADIUS = 3000
AX_OFFSET = 10000

colours = [[0, 0.4470, 0.7410], [0.8500, 0.3250, 0.0980],
          [0.9290, 0.6940, 0.1250], [0.4660, 0.6740, 0.1880],
          [0.3010, 0.7450, 0.9330], [0.6350, 0.0780, 0.1840]]


fig, ax = plt.subplots()
ax.set_xlim((-10000,10000))
ax.set_ylim((-10000,10000))
plt.xlabel('meters')
plt.ylabel('meters')
ax.set_aspect('equal')

Rs_0 = 2000
Rs_final = 7000
Rs = np.arange(Rs_0, Rs_final, Rs_0)
Phis = [0, np.pi/8, 0]

c = np.array(([(0,0)]))
c2 = np.array(([(0,0)]))

print("Antenna Position Calculation\n")
for i,r in enumerate(Rs):
    N = int(2**(i+2))
    theta_0 = 0
    theta_final = 2*np.pi
    thetas = np.arange(theta_0, theta_final, 2*np.pi/N)
    for thetai in thetas:
        xi = r*np.cos(thetai + Phis[i])
        yi = r*np.sin(thetai + Phis[i])
        ci = (xi,yi)
        c=np.vstack((c,ci))
        c2=np.vstack((c,ci))
    c=c[1:]
    for i,ci in enumerate(c):
        ax.add_artist(plt.Circle(ci, RADIUS, color='black', alpha=.3))
        ax.plot((ci[0]),(ci[1]), '.', color='white')

ax.plot((0),(0), '.', color='white', markersize=15)

print("Create Antennas Database\n")
create_ant_db(c2)
print("Update the Database\n")
sp.call(["make", "clean"], stdout=sp.DEVNULL, stderr=sp.DEVNULL)
sp.call(["make"])
print("Start Triangulation Calculations\n")
sp.call("./triangulation")
print("Read Results\n")
cnew, p = read_triangulation_output()

print("Visualise output\n")

for i,ci in enumerate(cnew):
    ax.add_artist(plt.Circle(ci, 3000, color=colours[i], alpha=.6))
    ax.plot((ci[0]),(ci[1]), '.', color='white')

ax.plot((p[0]),(p[1]), '+', color='white', markersize=11)

print("Save Figures\n")
fig.savefig('grid.png',transparent=True)
fig.savefig('grid.svg',transparent=True)
print("Application Sucesffuly Terminated\n")
