/******************************************************************************
 *  @FILE       antennas_defs.h
 *  @PROJECT    MARS LANDING SYSTEM SPACE APPS CHALLENGE 2018
 *  @SUBTASK	Positioning Software
 *  @TEAM       Thumbling's Travels
 *  @AUTHOR     Evangelos Petrongonas  (vpetrog) (vpetrog@yahoo.gr)
 *  @BRIEF      Custom Type Definitions for the antennas API
 *  @BUGS       No known bugs.
 *  @CHANGELOG
 *      -v0.2 Changed Data Types
 *      -v0.1 Initial Commit
 *****************************************************************************/
#ifndef ANTENNAS_DEFS_H
#define ANTENNAS_DEFS_H

#include "triangulation_defs.h"
#include "mls_types.h"

#define ANT_INT_ERR 100
#define ANT_INT_SUC 0

#define ANT_READ_ERR 1
#define ANT_READ_SUC 0

typedef unsigned char u8;

struct AntennaInfo {
    mlsAntNo noAnt;
    mlsAntId* antId;
    mlsAntRange* antRange;
    mlsAntDist* antDistance; /* NOT YET IMPLEMENTED */
    struct Points* antPos;
};

#endif
