/**************************************************************
				WARNING: THIS IS AN AUTO GENERATED FILE
**************************************************************/


#include "mls_types.h"

 #ifndef ANTENNAS_DB_H_
#define ANTENNAS_DB_H_

#define ANTENNA_ERR ((mlsGrid) 0)
#define TOTAL_ANTENNAS 28

/* ANTENNA LOCATIONS DEFINITIONS */

#define ANTENNA_X_0	(mlsGrid) 10000
#define ANTENNA_Y_0	(mlsGrid) 12000

#define ANTENNA_X_1	(mlsGrid) 8000
#define ANTENNA_Y_1	(mlsGrid) 10000

#define ANTENNA_X_2	(mlsGrid) 10000
#define ANTENNA_Y_2	(mlsGrid) 8000

#define ANTENNA_X_3	(mlsGrid) 13695
#define ANTENNA_Y_3	(mlsGrid) 11530

#define ANTENNA_X_4	(mlsGrid) 11530
#define ANTENNA_Y_4	(mlsGrid) 13695

#define ANTENNA_X_5	(mlsGrid) 8470
#define ANTENNA_Y_5	(mlsGrid) 13695

#define ANTENNA_X_6	(mlsGrid) 6305
#define ANTENNA_Y_6	(mlsGrid) 11530

#define ANTENNA_X_7	(mlsGrid) 6305
#define ANTENNA_Y_7	(mlsGrid) 8470

#define ANTENNA_X_8	(mlsGrid) 8470
#define ANTENNA_Y_8	(mlsGrid) 6305

#define ANTENNA_X_9	(mlsGrid) 11530
#define ANTENNA_Y_9	(mlsGrid) 6305

#define ANTENNA_X_10	(mlsGrid) 13695
#define ANTENNA_Y_10	(mlsGrid) 8470

#define ANTENNA_X_11	(mlsGrid) 16000
#define ANTENNA_Y_11	(mlsGrid) 10000

#define ANTENNA_X_12	(mlsGrid) 15543
#define ANTENNA_Y_12	(mlsGrid) 12296

#define ANTENNA_X_13	(mlsGrid) 14242
#define ANTENNA_Y_13	(mlsGrid) 14242

#define ANTENNA_X_14	(mlsGrid) 12296
#define ANTENNA_Y_14	(mlsGrid) 15543

#define ANTENNA_X_15	(mlsGrid) 10000
#define ANTENNA_Y_15	(mlsGrid) 16000

#define ANTENNA_X_16	(mlsGrid) 7704
#define ANTENNA_Y_16	(mlsGrid) 15543

#define ANTENNA_X_17	(mlsGrid) 5758
#define ANTENNA_Y_17	(mlsGrid) 14242

#define ANTENNA_X_18	(mlsGrid) 4457
#define ANTENNA_Y_18	(mlsGrid) 12296

#define ANTENNA_X_19	(mlsGrid) 4000
#define ANTENNA_Y_19	(mlsGrid) 10000

#define ANTENNA_X_20	(mlsGrid) 4457
#define ANTENNA_Y_20	(mlsGrid) 7704

#define ANTENNA_X_21	(mlsGrid) 5758
#define ANTENNA_Y_21	(mlsGrid) 5758

#define ANTENNA_X_22	(mlsGrid) 7704
#define ANTENNA_Y_22	(mlsGrid) 4457

#define ANTENNA_X_23	(mlsGrid) 10000
#define ANTENNA_Y_23	(mlsGrid) 4000

#define ANTENNA_X_24	(mlsGrid) 12296
#define ANTENNA_Y_24	(mlsGrid) 4457

#define ANTENNA_X_25	(mlsGrid) 14242
#define ANTENNA_Y_25	(mlsGrid) 5758

#define ANTENNA_X_26	(mlsGrid) 15543
#define ANTENNA_Y_26	(mlsGrid) 7704

#define ANTENNA_X_27	(mlsGrid) 15543
#define ANTENNA_Y_27	(mlsGrid) 7704


/* ANTENNA RANGE DEFINITIONS */
#define ANTENNA_RANGE_0	(mlsAntRange) 3000
#define ANTENNA_RANGE_1	(mlsAntRange) 3000
#define ANTENNA_RANGE_2	(mlsAntRange) 3000
#define ANTENNA_RANGE_3	(mlsAntRange) 3000
#define ANTENNA_RANGE_4	(mlsAntRange) 3000
#define ANTENNA_RANGE_5	(mlsAntRange) 3000
#define ANTENNA_RANGE_6	(mlsAntRange) 3000
#define ANTENNA_RANGE_7	(mlsAntRange) 3000
#define ANTENNA_RANGE_8	(mlsAntRange) 3000
#define ANTENNA_RANGE_9	(mlsAntRange) 3000
#define ANTENNA_RANGE_10	(mlsAntRange) 3000
#define ANTENNA_RANGE_11	(mlsAntRange) 3000
#define ANTENNA_RANGE_12	(mlsAntRange) 3000
#define ANTENNA_RANGE_13	(mlsAntRange) 3000
#define ANTENNA_RANGE_14	(mlsAntRange) 3000
#define ANTENNA_RANGE_15	(mlsAntRange) 3000
#define ANTENNA_RANGE_16	(mlsAntRange) 3000
#define ANTENNA_RANGE_17	(mlsAntRange) 3000
#define ANTENNA_RANGE_18	(mlsAntRange) 3000
#define ANTENNA_RANGE_19	(mlsAntRange) 3000
#define ANTENNA_RANGE_20	(mlsAntRange) 3000
#define ANTENNA_RANGE_21	(mlsAntRange) 3000
#define ANTENNA_RANGE_22	(mlsAntRange) 3000
#define ANTENNA_RANGE_23	(mlsAntRange) 3000
#define ANTENNA_RANGE_24	(mlsAntRange) 3000
#define ANTENNA_RANGE_25	(mlsAntRange) 3000
#define ANTENNA_RANGE_26	(mlsAntRange) 3000
#define ANTENNA_RANGE_27	(mlsAntRange) 3000


/* ANTENNA ID DEFINITIONS */
#define ANTENNA_ID_0	(mlsAntId) 1
#define ANTENNA_ID_1	(mlsAntId) 2
#define ANTENNA_ID_2	(mlsAntId) 4
#define ANTENNA_ID_3	(mlsAntId) 8
#define ANTENNA_ID_4	(mlsAntId) 16
#define ANTENNA_ID_5	(mlsAntId) 32
#define ANTENNA_ID_6	(mlsAntId) 64
#define ANTENNA_ID_7	(mlsAntId) 128
#define ANTENNA_ID_8	(mlsAntId) 256
#define ANTENNA_ID_9	(mlsAntId) 512
#define ANTENNA_ID_10	(mlsAntId) 1024
#define ANTENNA_ID_11	(mlsAntId) 2048
#define ANTENNA_ID_12	(mlsAntId) 4096
#define ANTENNA_ID_13	(mlsAntId) 8192
#define ANTENNA_ID_14	(mlsAntId) 16384
#define ANTENNA_ID_15	(mlsAntId) 32768
#define ANTENNA_ID_16	(mlsAntId) 65536
#define ANTENNA_ID_17	(mlsAntId) 131072
#define ANTENNA_ID_18	(mlsAntId) 262144
#define ANTENNA_ID_19	(mlsAntId) 524288
#define ANTENNA_ID_20	(mlsAntId) 1048576
#define ANTENNA_ID_21	(mlsAntId) 2097152
#define ANTENNA_ID_22	(mlsAntId) 4194304
#define ANTENNA_ID_23	(mlsAntId) 8388608
#define ANTENNA_ID_24	(mlsAntId) 16777216
#define ANTENNA_ID_25	(mlsAntId) 33554432
#define ANTENNA_ID_26	(mlsAntId) 67108864
#define ANTENNA_ID_27	(mlsAntId) 134217728
#endif
