/******************************************************************************
 *  @FILE       triangulation.c
 *  @PROJECT    MARS LANDING SYSTEM SPACE APPS CHALLENGE 2018
 *  @SUBTASK	Positioning Software
 *  @TEAM       Thumbling's Travels
 *  @AUTHOR     Evangelos Petrongonas  (vpetrog) (vpetrog@yahoo.gr)
 *  @BRIEF      Implementation of the triangulation API
 *  @BUGS       No known bugs.
 *  @CHANGELOG
 *  	-v0.2 Added a decode function. Added Custom Data Types. Changeed the
 *  	calculation of the point from a median to an average approach
 *      -v0.1 Initial Commit
 *****************************************************************************/

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "triangulation.h"

#define DO_PRAGMA(x) _Pragma (#x)
#define TODO(x) DO_PRAGMA(message ("TODO - " #x))

typedef struct Points point;

void initialise_map(const mlsGrid MAX_X, const mlsGrid MAX_Y, mlsGrid map[MAX_Y][MAX_X])
{
	memset(map, 0, MAX_X * MAX_Y * sizeof(mlsGrid));
	/*
	for (size_t i = 0; i < MAX_Y; i++)
		for (size_t j = 0; j < MAX_X; j++)
			map[i][j] = 0;
	*/
}

void print_map(const mlsGrid MAX_X, const mlsGrid MAX_Y, mlsGrid map[MAX_Y][MAX_X])
{
	FILE *out_file = fopen("map.dat", "w");
	for (size_t i = 0; i < MAX_Y; i++) {
		fprintf(out_file, "\n");
		for (size_t j = 0; j < MAX_X; j++) {
			fprintf(out_file,"%u ", map[i][j]);
		}
	}
}


float distance(point antenna, point p)
{
		int i,j ;
		i = pow( (int) (antenna.x) - (int) p.x , 2);
		j = pow( (int) (antenna.y) - (int) p.y , 2);
		return sqrt(i + j);
}

void create_map( struct AntennaInfo antInfo, const mlsGrid MAX_X, const mlsGrid MAX_Y,
			     mlsGrid map[MAX_X][MAX_Y])
{
	mlsAntNo noAnt = antInfo.noAnt;
	point* antennas = antInfo.antPos;
	mlsAntId* antId = antInfo.antId;
	mlsAntRange* antRange = antInfo.antRange;
	for (size_t y = 0; y < MAX_Y; y++) {
		for (size_t x = 0; x < MAX_X; x++) {
			point temp = {x,y};
			for (size_t k = 0; k < noAnt; k++) {
				if (distance(antennas[k], temp) < antRange[k]) {
					/* Perhaps an encodinf should be used ? */
					map[y][x] |= antId[k];
				}
			}
		}
	}
}

point calculate_position(const mlsGrid MAX_X, const mlsGrid MAX_Y, mlsGrid map[MAX_X][MAX_Y])
{

	mlsGrid avx = 0; mlsGrid avy = 0;
	mlsGrid cnt = 0;

	for (mlsGrid i = 0; i < MAX_Y; i++)
		for (mlsGrid j = 0; j < MAX_X; j++)
			if (_decode_ant_number(map[i][j]) == 4){
				avx +=j;
				avy +=i;
				cnt++;
			}

	mlsGrid x_loc = (mlsGrid) ((avx * 1.0)/(cnt));
	mlsGrid y_loc = (mlsGrid) ((avy * 1.0)/(cnt));
	point p = {x_loc, y_loc};
	// optional
	// map[y_loc][x_loc] = (mlsGrid) 100;

	return p;
}

void print_output(point p, point* antennas, mlsAntNo noAnt, mlsAntRange* antRange)
{
	FILE *out_file = fopen("c_out.dat", "w");
	fprintf(out_file, "L %d %d\n", p.x, p.y);
	for (size_t i = 0; i < noAnt; i++) {
		fprintf(out_file, "A%ld %d %d %d\n",i, antennas[i].x, antennas[i].y, antRange[i]);
	}
}

mlsAntNo _decode_ant_number(mlsGrid i)
{
	/*
	 * Warning this function is implemented stricted for an up to 32
	 * size antenna network
	 */
	i = i - ((i >> 1) & 0x55555555);
  	i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
  	return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
}
