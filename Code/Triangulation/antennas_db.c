/******************************************************************************
 *  @FILE       antennas_db.c
 *  @PROJECT    MARS LANDING SYSTEM SPACE APPS CHALLENGE 2018
 *  @SUBTASK	Positioning Software
 *  @TEAM       Thumbling's Travels
 *  @AUTHOR     Evangelos Petrongonas  (vpetrog) (vpetrog@yahoo.gr)
 *  @BRIEF      Creates the object file for the Antenna Database
 *  @BUGS       No known bugs.
 *  @CHANGELOG
 *      -v0.2 Compatibility for the new DataType System
 *      -v0.1 Initial Commit
 *****************************************************************************/

#include "antennas_db.h"
#include "triangulation_defs.h"

struct Points ANT_POSITION_DB[] = {{ANTENNA_X_0, ANTENNA_Y_0},
                                     {ANTENNA_X_1, ANTENNA_Y_1},
                                     {ANTENNA_X_2, ANTENNA_Y_2},
                                     {ANTENNA_X_3, ANTENNA_Y_3},
                                     {ANTENNA_X_4, ANTENNA_Y_4} ,
                                     {ANTENNA_X_5, ANTENNA_Y_5 },
                                     {ANTENNA_X_6, ANTENNA_Y_6 },
                                     {ANTENNA_X_7, ANTENNA_Y_7 },
                                     {ANTENNA_X_8, ANTENNA_Y_8 },
                                     {ANTENNA_X_9, ANTENNA_Y_9 },
                                     {ANTENNA_X_10, ANTENNA_Y_10 },
                                     {ANTENNA_X_11, ANTENNA_Y_11 },
                                     {ANTENNA_X_12, ANTENNA_Y_12 },
                                     {ANTENNA_X_13, ANTENNA_Y_13 },
                                     {ANTENNA_X_14, ANTENNA_Y_14 },
                                     {ANTENNA_X_15, ANTENNA_Y_15 },
                                     {ANTENNA_X_16, ANTENNA_Y_16 },
                                     {ANTENNA_X_17, ANTENNA_Y_17 },
                                     {ANTENNA_X_18, ANTENNA_Y_18 },
                                     {ANTENNA_X_19, ANTENNA_Y_19 },
                                     {ANTENNA_X_20, ANTENNA_Y_20 },
                                     {ANTENNA_X_21, ANTENNA_Y_21 },
                                     {ANTENNA_X_22, ANTENNA_Y_22 },
                                     {ANTENNA_X_23, ANTENNA_Y_23 },
                                     {ANTENNA_X_24, ANTENNA_Y_24 },
                                     {ANTENNA_X_25, ANTENNA_Y_25 },
                                     {ANTENNA_X_26, ANTENNA_Y_26 },
                                     {ANTENNA_X_27, ANTENNA_Y_27 }

                                 };

mlsAntRange ANT_RANGE_DB[] = { ANTENNA_RANGE_0,
                               ANTENNA_RANGE_1,
                               ANTENNA_RANGE_2,
                               ANTENNA_RANGE_3,
                               ANTENNA_RANGE_4,
                               ANTENNA_RANGE_5,
                               ANTENNA_RANGE_6,
                               ANTENNA_RANGE_7,
                               ANTENNA_RANGE_8,
                               ANTENNA_RANGE_9,
                               ANTENNA_RANGE_10,
                               ANTENNA_RANGE_11,
                               ANTENNA_RANGE_12,
                               ANTENNA_RANGE_13,
                               ANTENNA_RANGE_14,
                               ANTENNA_RANGE_15,
                               ANTENNA_RANGE_16,
                               ANTENNA_RANGE_17,
                               ANTENNA_RANGE_18,
                               ANTENNA_RANGE_19,
                               ANTENNA_RANGE_20,
                               ANTENNA_RANGE_21,
                               ANTENNA_RANGE_22,
                               ANTENNA_RANGE_23,
                               ANTENNA_RANGE_24,
                               ANTENNA_RANGE_25,
                               ANTENNA_RANGE_26,
                               ANTENNA_RANGE_27
                             };

mlsAntId ANT_ID_DB[] = { ANTENNA_ID_0,
                         ANTENNA_ID_1,
                         ANTENNA_ID_2,
                         ANTENNA_ID_3,
                         ANTENNA_ID_4,
                         ANTENNA_ID_5,
                         ANTENNA_ID_6,
                         ANTENNA_ID_7,
                         ANTENNA_ID_8,
                         ANTENNA_ID_9,
                         ANTENNA_ID_10,
                         ANTENNA_ID_11,
                         ANTENNA_ID_12,
                         ANTENNA_ID_13,
                         ANTENNA_ID_14,
                         ANTENNA_ID_15,
                         ANTENNA_ID_16,
                         ANTENNA_ID_17,
                         ANTENNA_ID_18,
                         ANTENNA_ID_19,
                         ANTENNA_ID_20,
                         ANTENNA_ID_21,
                         ANTENNA_ID_22,
                         ANTENNA_ID_23,
                         ANTENNA_ID_24,
                         ANTENNA_ID_25,
                         ANTENNA_ID_26,
                         ANTENNA_ID_27
                     };
