/******************************************************************************
 *  @FILE       triangulation_defs.h
 *  @PROJECT    MARS LANDING SYSTEM SPACE APPS CHALLENGE 2018
 *  @SUBTASK	Positioning Software
 *  @TEAM       Thumbling's Travels
 *  @AUTHOR     Evangelos Petrongonas  (vpetrog) (vpetrog@yahoo.gr)
 *  @BRIEF      Custom Type Definitions for the triangulation API
 *  @BUGS       No known bugs.
 *  @CHANGELOG
 *      -v0.2 Changed Data Types
 *      -v0.1 Initial Commit
 *****************************************************************************/
#ifndef TRIANGULATION_DEFS_H_
#define TRIANGULATION_DEFS_H_

#include "mls_types.h"

typedef unsigned char u8;

struct Points {
	mlsGrid x;
	mlsGrid y;
};

#endif
