/******************************************************************************
 *  @FILE       antennas.c
 *  @PROJECT    MARS LANDING SYSTEM SPACE APPS CHALLENGE 2018
 *  @SUBTASK	Positioning Software
 *  @TEAM       Thumbling's Travels
 *  @AUTHOR     Evangelos Petrongonas  (vpetrog) (vpetrog@yahoo.gr)
 *  @BRIEF      Implementation of the antenna's API
 *  @BUGS       No known bugs.
 *  @CHANGELOG
 *      -v0.2 Changed Data Types
 *      -v0.1 Initial Commit
 *****************************************************************************/

#include <stdlib.h>
#include "antennas.h"
#include "antennas_db.h"
#include <stdio.h>

typedef struct Points points;
typedef struct AntennaInfo antInfo;
typedef unsigned char u8;

#define DEBUG

#ifdef DEBUG
#define DEBUG_PRINT 1
#else
#define DEBUG_PRINT 0
#endif

#define DEBUGP(fmt, ...) \
        do { if (DEBUG_PRINT) fprintf(stderr, fmt, __VA_ARGS__); } while (0)

#define NO_ANTENNAS 4

extern points ANT_POSITION_DB[];
extern mlsAntRange ANT_RANGE_DB[];
extern mlsAntId ANT_ID_DB[];

int _get_antennaPosition(mlsGrid i, points* p)
{
    if(i > TOTAL_ANTENNAS){
        DEBUGP("_get_antennaPosition at %d", i);
        return ANT_INT_ERR;
    }
    else {
        *p = ANT_POSITION_DB[i];
        return ANT_INT_SUC;
    }
}

mlsAntRange _get_antennaRange(mlsGrid i) {
    if(i > TOTAL_ANTENNAS) {
        DEBUGP("_get_antennaRange at %d", i);
        return ANT_INT_ERR;
    }
    else
        return ANT_RANGE_DB[i];
}

mlsAntId _get_antennaId(mlsGrid i) {
    if(i > TOTAL_ANTENNAS) {
        DEBUGP("_get_antennaId at %d", i);
        return ANT_INT_ERR;
    } else {
        return ANT_ID_DB[i];
    }
}

int read_antennas(antInfo* antennas)
{

    /*
    * The following code is temporally. In the final programma the data will
    * be read from a file or a bus
    */
    mlsAntNo noAntennas = NO_ANTENNAS;
    antennas->noAnt = noAntennas;
    antennas->antPos = (points*) malloc(noAntennas * sizeof(points));
    antennas->antRange = (mlsAntRange*) malloc(noAntennas * sizeof(mlsAntRange));
    antennas->antId = (mlsAntId*) malloc(noAntennas * sizeof(mlsAntId));

    mlsAntNo ant[] = {1, 8, 7, 6};
    for (size_t i = 0; i < noAntennas; i++) {
        if(_get_antennaPosition(ant[i], &(antennas->antPos[i])) == ANT_INT_ERR){
            DEBUGP("read_antennas Position at %lu", i);
            return ANT_READ_ERR;
        }
        antennas->antRange[i] = _get_antennaRange(ant[i]);
        if(antennas->antRange[i] == ANT_INT_ERR){
            DEBUGP("read_antennas Range at %lu", i);
            return ANT_READ_ERR;
        }
        antennas->antId[i] = _get_antennaId(ant[i]);
        if(antennas->antId[i] == ANT_INT_ERR){
            DEBUGP("read_antennas ID at %lu", i);
            return ANT_READ_ERR;
        }
    }
    return ANT_READ_SUC;
}
