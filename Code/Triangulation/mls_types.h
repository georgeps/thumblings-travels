/******************************************************************************
 *  @FILE       mls_types.h
 *  @PROJECT    MARS LANDING SYSTEM SPACE APPS CHALLENGE 2018
 *  @SUBTASK	Positioning Software
 *  @TEAM       Thumbling's Travels
 *  @AUTHOR     Evangelos Petrongonas  (vpetrog) (vpetrog@yahoo.gr)
 *  @BRIEF      defines the data_types used by the application
 *  @BUGS       No known bugs.
 *  @CHANGELOG
 *      -v0.1 Initial Commit
 *****************************************************************************/

#ifndef MLS_TYPS_H
#define MLS_TYPS_H
#include <stdint.h>

typedef uint16_t	mlsAntRange;
typedef uint32_t	mlsAntId;
typedef float		mlsAntDist;
typedef uint32_t	mlsGrid;
typedef	uint8_t		mlsAntNo;

#endif
