/******************************************************************************
 *  @FILE       main.c
 *  @PROJECT    MARS LANDING SYSTEM SPACE APPS CHALLENGE 2018
 *  @SUBTASK	Positioning Software
 *  @TEAM       Thumbling's Travels
 *  @AUTHOR     Evangelos Petrongonas  (vpetrog) (vpetrog@yahoo.gr)
 *  @BRIEF      main code
 *  @BUGS       No known bugs.
 *  @CHANGELOG
 *      -v0.2 Compatibility for the new datatype version
 *      -v0.1 Initial Commit
 *****************************************************************************/
#include <stdio.h>

#include "triangulation.h"
#include "antennas.h"

#define DEBUG

#ifdef DEBUG
#define DEBUG_PRINT 1
#else
#define DEBUG_PRINT 0
#endif

#define DEBUGP(fmt, ...) \
        do { if (DEBUG_PRINT) fprintf(stderr, fmt, __VA_ARGS__); } while (0)

#define MAX_X	(mlsGrid) 10000
#define MAX_Y	(mlsGrid) 10000

typedef struct Points points;
typedef struct AntennaInfo antennaInfo;

int main(void) {
	DEBUGP("%s\n","Starting Trianglulation Programm");
	points result;
	static mlsGrid map[MAX_X][MAX_Y];
	antennaInfo antInfo;
	DEBUGP("%s\n","Initialising Map");
	initialise_map(MAX_X, MAX_Y, map);
	DEBUGP("%s\n","Read Antennas");
	if(read_antennas(&antInfo) == ANT_READ_ERR){
		return 1;
	}
	DEBUGP("%s\n","Creating Map");
	create_map(antInfo, MAX_X, MAX_Y, map);
	DEBUGP("%s\n","Calculate Result");
	result = calculate_position(MAX_X, MAX_Y, map);
	//print_map(MAX_X, MAX_Y, map); // Warning it genreates large files (~256MB)
    DEBUGP("%s\n","Print Output");
	print_output(result, antInfo.antPos, antInfo.noAnt, antInfo.antRange);
    DEBUGP("%s\n","Return control to Visualiser");
	return 0;
}
