/******************************************************************************
 *  @FILE       antennas.h
 *  @PROJECT    MARS LANDING SYSTEM SPACE APPS CHALLENGE 2018
 *  @SUBTASK	Positioning Software
 *  @TEAM       Thumbling's Travels
 *  @AUTHOR     Evangelos Petrongonas  (vpetrog) (vpetrog@yahoo.gr)
 *  @BRIEF      Definition and Documentation of the antenna API
 *  @BUGS       The read antennas function currently can not read data
 *              from external sources and it only produces a demo file
 *  @CHANGELOG
 *      -v0.2 Changed Data Types
 *      -v0.1 Initial Commit
 *****************************************************************************/
#ifndef ANTENNAS_H_
#define ANTENNAS_H_

#include "triangulation_defs.h"
#include "antennas_defs.h"

typedef unsigned char u8;

/**
 * read_antennas creates a struct holding all the information of the antennas
 * that are within the asrtonauts range
 * @param   antennas the struct to be filed
 * @return  - ANT_READ_ERR on error
 *          - ANT_READ_SUC on success
 */
int read_antennas(struct AntennaInfo* antennas);

/*******************************************************************************
The following functions are for internal use and Low Level API
*******************************************************************************/
/**
 * _get_antennaPosition is used to extract an antennas' exact position from
 * the database
 * @param  i antenna Node id
 * @param  p the antennas' positions
 * @return  - ANT_INT_ERR on error
 *          - ANT_INT_SUC on success
 */
int _get_antennaPosition(mlsGrid i, struct Points * p);

/**
 * _get_antennaRange is used to extract an antennas' Range from the database
 * @param  i antenna Node id
 * @return  - ANT_INT_ERR on error
 *          - antennas' range on success
 */
mlsAntRange _get_antennaRange(mlsGrid i);

/**
 * _get_antennaId is used to extract an antennas' Cpverage ID from the database
 * @param  i antenna Node id
 * @return  - ANT_INT_ERR on error
 *          - antennas' Coverage ID on success
 */
mlsAntId _get_antennaId(mlsGrid i);
#endif
