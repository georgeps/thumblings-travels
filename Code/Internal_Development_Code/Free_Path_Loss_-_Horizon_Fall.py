import numpy as np

#Antenna radius in km
d=5
#Frequency in MHz
f=433
print ('Free Space Path Loss in', d, 'km radius')
#free space path loss (FSPL) in dB
FSPL = 20*np.log10(d) + 20*np.log10(f) + 32.45
print(FSPL,"dB\n")

#Distance in km
l=5
print('Horizon Fall in', l, 'km radius')

#Fall in km
v = 1695*((np.sin(l/6780))**2)
#Fall in cm
x = v*100000
print (x,"cm")

